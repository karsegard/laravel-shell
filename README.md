# This is my package laravel-shell

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-shell.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-shell)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-shell.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-shell)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-shell
```


## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
