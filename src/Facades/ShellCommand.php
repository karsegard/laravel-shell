<?php

namespace KDA\Laravel\Shell\Facades;

use Illuminate\Support\Facades\Facade;

class ShellCommand extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
