<?php
namespace KDA\Laravel\Shell;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Shell\Facades\ShellCommand as Facade;
use KDA\Laravel\Shell\ShellCommand as Library;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    protected $packageName ='laravel-shell';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
