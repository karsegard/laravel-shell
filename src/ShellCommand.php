<?php
namespace KDA\Laravel\Shell;
use Symfony\Component\Process\Process;

//use Illuminate\Support\Facades\Blade;
class ShellCommand 
{
    public function execute($cmd): string
    {
        $process = Process::fromShellCommandline($cmd);

        $processOutput = '';

        $captureOutput = function ($type, $line) use (&$processOutput) {
            $processOutput .= $line;
        };

        $process->setTimeout(null)
            ->run($captureOutput);

        if ($process->getExitCode()) {
            $exception = new \Exception($processOutput);
            report($exception);

            throw $exception;
        }

        return $processOutput;
    }
}